import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class Bird extends Animal {

    @Override
    public List<Feed> getFeed() {
        List<Feed> ptakLubi = new LinkedList<>();

        ptakLubi.add(Feed.ZIARNO);
        ptakLubi.add(Feed.WODA);

        return ptakLubi;
    }
}
