import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public abstract class Animal {

    List<Integer> allergy;
    List<KindOfFood> kindOfFood;

    private int age;
    private String name;
    private Enum typ;



    public boolean feedMe(Feed feed){
//pozniej dopisac instrukcje warunkowe w zaleznosci od np. pory dnia lub zwierzecia
        return true;
    }

    public abstract List<Feed> getFeed();

    public List<Integer> getAllergy() {
        return allergy;
    }

    public void setAllergy(List<Integer> allergy) {
        this.allergy = allergy;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Enum getTyp() {
        return typ;
    }

    public void setTyp(Enum typ) {
        this.typ = typ;
    }
}
/*
zadanie na ten czas
stworzyc obiekty, osobe jakas, kilka zwierzat, dodac zwierzeta
do osoby, metody, ktore dodaja zwierzeta do osoby do zwierzecia dodajemy
rzeczy ktore lubi jest(moze byuc w construkctoreze)
sprobowac je wylistowac i nakarmic

dalej:
tworzymy dwie osoby (w main) i przypisujemy jej zwierzeta i probujemy je nakarmic poprzednimi metodami
 */