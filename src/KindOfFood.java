import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class KindOfFood implements Comparable<KindOfFood> {

    Feed feed;
    //ile jednostek posiada ten pokarm
    List<Feed> myFoodForAnimal;
    int quantity;

    //konstruktor
    public KindOfFood(Feed feed) {
        this.feed = feed;
    }

    public List<Feed> getMyFoodForAnimal() {
        return myFoodForAnimal;
    }

    public void setMyFoodForAnimal(List<Feed> myFoodForAnimal) {
        this.myFoodForAnimal = myFoodForAnimal;
    }
//getter
    public Feed getFeed() {
        return feed;
    }

    @Override
    public int compareTo(KindOfFood o) {
        int result = -1;

        if(getFeed() == o.getFeed()){
            result = 0;
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        boolean result = false;

        if (obj instanceof KindOfFood) {

            KindOfFood kindOfFood = (KindOfFood) obj;

            if (getFeed() == kindOfFood.getFeed()) {
                result = true;
            }
        }
        return result;
    }
}
//caly kind of feed z zajec