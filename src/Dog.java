import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class Dog extends Animal {
    @Override
    public List<Feed> getFeed() {
        List<Feed> piesLubi = new LinkedList<>();

        piesLubi.add(Feed.MIESO);
        piesLubi.add(Feed.MLEKO);
        piesLubi.add(Feed.WODA);

        return piesLubi;
    }
}
