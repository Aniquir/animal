import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class Main {

    public static void main(String[] args) {


        KindOfFood food = new KindOfFood(Feed.MLEKO);
        KindOfFood food2 = new KindOfFood(Feed.MLEKO);

        if (food.compareTo(food2) == 0){
            System.out.println("mam to jedzenie");
        }else {
            System.out.println("idz do sklepu");
        }

        if (food.equals(food2)){
            System.out.println("takie same");
        }else {
            System.out.println("rozne");
        }
        System.out.println();

        //utworzenie nowej osoby
        Person hieronim = new Person();
        //zdefiniowanie adresu i jego cech(ponieważ jest zrobiony w oddzielnej klasie)
        Adres adresHieronima = new Adres();
        adresHieronima.setUlica("ul.Zana");
        adresHieronima.setNumerDomu(11);
        adresHieronima.setMiasto("Lublin");
        //przypisanie parametrow do osoby definiowanych w klasie Person
        hieronim.setName("Hieronim");
        hieronim.setSurName("Kowalski");
        hieronim.setAge(43);
        hieronim.setAdres(adresHieronima);
        hieronim.setSex(Plec.MALE);


        //zdefiniowanie i przypisanie cech zwierzetom

        //kot
        Cat cat = new Cat();
        //cechy kota
        cat.setAge(2);
        cat.setName("Filemon");
        cat.setTyp(TypeAnimal.MAMMAL);

        //pies
        Dog dog = new Dog();
        //cechy psa
        dog.setAge(5);
        dog.setName("Azor");
        dog.setTyp(TypeAnimal.MAMMAL);

        //ptak
        Bird bird = new Bird();
        //cechy ptaka
        bird.setAge(1);
        bird.setName("Elemelek");
        bird.setTyp(TypeAnimal.BIRD);

        //ryba
        Fish fish = new Fish();
        //cechy ryby
        fish.setAge(20);
        fish.setName("Ari");
        fish.setTyp(TypeAnimal.FISH);

        //dziala takie drukowanie v
        //System.out.println(cat.getAge());

        //zdefiniowanie samej listy (bez zwierzat) osoby
        List<Animal> hieAnimals = new LinkedList<>();
        //dodawanie zwierzat do nowo utworzonej listy
        hieAnimals.add(cat);
        hieAnimals.add(dog);
        hieAnimals.add(bird);
        hieAnimals.add(fish);

        // przypisanie nowo utworzonej listy do listy hieronima(zdefiniowanej w klasie Person)
        hieronim.setMyAnimals(hieAnimals);


        //tworzenie listy z jedzeniem jakie posiada osoba
        List<Feed> hierMyFoodForAnimal = new LinkedList<>();

        //dodawanie produktow do listy z jedzeniem podanej osoby
        hierMyFoodForAnimal.add(Feed.MIESO);
        hierMyFoodForAnimal.add(Feed.MLEKO);
        hierMyFoodForAnimal.add(Feed.PLANKTON);
        hierMyFoodForAnimal.add(Feed.SUCHY_POKARM);
        hierMyFoodForAnimal.add(Feed.WODA);
        hierMyFoodForAnimal.add(Feed.ZIARNO);

        hieronim.setMyFoodForAnimal(hierMyFoodForAnimal);

        hieronim.feedAnimal(hieAnimals);



    }

}
/*
dokonczyc projekt w domu:
- zle drukuje (poprawic w pierwszej kolejnosci)

18.08.2017
- wszystko ma sie wyswietlac i informowac o stanie projektu
- stworzyc plik JSON dla tego projektu
- dodac numerowanie do jedzenia, ze na przyklad mamy 10x mieso, pies chce 2 (sprawdzamy ile mamy)
  i zostaje 8 (odejmuje od naszych zasobow) jak nie ma to metoda gotoShop(jeszcze nie zaimplementowana) ale sie wyswietla,
  ze trzeba pojsc do sklepu (sprawdzamy czy mamy jedzenie, na sztuki, jezeli np. mamy 1
  a on chce 2 to jedna mu dajemy, a potem wyswietla, ze trzeba pojsc do sklepu)
- metoda goToShop (piszemy ja w klasie persondodaje powiedzmy po 5 produktow
  danego typu (wszystkie typy uzupelnia lub dany co brakuje po
  nakarmieniu wszystkich zwierzat
- ogolnie rozwijac projekt w domu
~ ogolnie to zrobic tak, ze zwierze go jakis okres czasu chce jedzenie (np. co 60 sekund chce jedno mleko) a my
  je karmimy co 5 minut (ma to byc w ~dwoch petlach)

 */
