import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class Fish extends Animal {

    @Override
    public List<Feed> getFeed() {
        List<Feed> rybaLubi = new LinkedList<>();

        rybaLubi.add(Feed.PLANKTON);
        rybaLubi.add(Feed.SUCHY_POKARM);

        return rybaLubi;
    }
}
